// Test case for c02ex09
// A WORD IS AN ALPHANUMERIC CHARACTER 
#include <stdio.h>

char *ft_strcapitalize(char *str);

int main()
{
    char str[] = "salut, comment tu vas ? 42mots quarante-deux; cinquante+et+un";
    printf("Before capitalize: %s\n", str);
    printf("Returns: %s\n", ft_strcapitalize(str));
    printf("After uppercase: %s", str);
}
